import java.util.Scanner;

public class HelloWorld {
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        System.out.print("The first number is:");
        int firstNum = userIn.nextInt();
        System.out.print("The second number is:");
        int secondNum = userIn.nextInt();
        userIn.close();
        System.out.println(firstNum + secondNum);
    }
}
